/*================================[driver.c]==========================================================

 * Copyright 2019 Agustín Rey <agustinrey61@gmail.com> - Yoel López <lopez.yoel25@gmail.com>
 * All rights reserved.
 * Version: 1.0.0
 * Fecha de creacion: 2019/07/15
 */

/*============================[Inclusiones de dependencias de funciones publicas]===================*/

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"

#include "sapi.h"
#include "sapi_uart.h"
#include "task.h"

/*==================[inclusiones - bibliotecas propias]============================================*/

#include "Driver.h"
#include "service.h"

/*==================[definiciones y macros]========================================================*/
#define TX_BIT    0x01
#define RX_BIT    0x02

/*==================[definiciones de datos internos]=========================*/

/* Variable de estado de la FSM de recepción del paquete por UART */
static rxStatus_t rxStatus = WAIT_SOF;
uint8_t *pDataToSend;
packet_t paquete;
static uint8_t DataToSend[LEN];
static buff_t rxbuff;

static infoDriverRx_t infoDriver;


/*==================[definiciones de datos externos]===============================================*/

extern QueueHandle_t queueRecieveUart;
extern QueueHandle_t queueTransmitUart;
SemaphoreHandle_t xSemNewPacket;
extern TaskHandle_t xHandlePrintQueue;
/*==================[declaraciones de funciones internas]=========================================*/

static void onRx(void *noUsado);

/*==================[declaraciones de funciones externas]=======================================*/

/*==================[definiciones de  funciones públicas]======================================*/

/* Inicializa la UART, habilita las interrupciones y crear las estructuras de las colas para el envío y repcepción
 * por la UART
 * Esta tarea es "One Shot" y se suicida
 *
 */

void Initialize_UART(void* taskParmPtr)
{
	if ((xSemNewPacket = xSemaphoreCreateBinary()) == NULL)
		printf("Error al crear semaforo NewPacket");

	/* Inicializar la UART_USB junto con las interrupciones de Tx y Rx */

	uartConfig(UART_USB, VX_BAUDIOS);

	/* Setea un callback al evento de recepción y habilito su interrupcion*/

	uartCallbackSet(UART_USB, UART_RECEIVE, onRx, NULL);

	/* Habilito todas las interrupciones de UART_USB*/

	uartInterrupt(UART_USB, true);

	/* Creación de colas para el envío y repcepción de información por UART */

	if ((queueRecieveUart = xQueueCreate(1, sizeof(rxbuff))) == NULL)
		printf("error al crear la queue queueRecieveUart");

	if ((queueTransmitUart = xQueueCreate(1, sizeof(DataToSend))) == NULL)
		printf("error al crear la queue queueTransmitUart");

	/*
	 *Se queda esperando que se libere el semáforo que indica que se recibió un paquete
	 *y lo envía por la cola al servicio
	 */
	for (;;)
	{
		if ( xSemaphoreTake( xSemNewPacket, portMAX_DELAY) == pdTRUE)
		{
			if (xQueueSend(queueRecieveUart, (void *) &rxbuff,1) != pdTRUE)
			{
				printf("error al enviar por cola queueRecieveUart");
			}

		}
	}
}

/*==================[definiciones de funciones internas]=====================*/

/* Función que se ejecuta con la interrupción de dato recibido por la UART
 * Implementa FSM para llenar el paquete con los datos recibidos
 * Al detectar un EOF libera el semáforo para que la tarea principal envíe la cola con el mensaje
 */

static void onRx(void *noUsado)
{
	static uint8_t contador;
	uint8_t datoRecibido, i;
	infoDriverRx_t *pinfo;
	uint32_t tiempo_medido;

	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xHigherPriorityTaskWoken = pdFALSE;

	while (uartRxReady(UART_USB))
	{
		tiempo_medido = xTaskGetTickCount();
		datoRecibido = uartRxRead(UART_USB);

		switch (rxStatus)
		{
		case WAIT_SOF: /* espera por el SOF */
			if (datoRecibido == SOF_) /*si el dato recibido es SOF inicia contador y pasa a esperar el EOF*/
			{
				infoDriver.tiempo_recepcion_ETX = tiempo_medido;
				rxbuff.largo = 0;
				rxStatus = WAIT_EOF;
			}
			break;

		case WAIT_EOF:
			switch (datoRecibido)
			{
			case EOF_: //acá recibió el EOF y la longitud esta dentro de los límites, entonces carga la estructura paquete
				if (rxbuff.largo < LEN)
				{
					infoDriver.tiempo_recepcion_STX = tiempo_medido;	//registra el tiempo de recepcion del STX

					memcpy(&rxbuff.buff[rxbuff.largo],&infoDriver,sizeof(infoDriver));

					xSemaphoreGiveFromISR(xSemNewPacket,
							&xHigherPriorityTaskWoken);
					rxStatus = WAIT_SOF;			//vuelve a esperar el SOF
				}
				break;

			case SOF_://recibe SOF antes de recibir el EOF, reinicia la recepción del paquete
				rxbuff.largo = 0;
				break;
			default:
				if (rxbuff.largo < LEN)
					rxbuff.buff[rxbuff.largo++] = datoRecibido;
				else
					rxStatus = WAIT_SOF;//la longitud es excesiva, reinicia la FSM a esperar el SOF
				break;
			}
		}
		portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
	}
}

//---- Se ejecuta cada vez que hay una interrupción por buffer de envío vacío
// envía todos los caracteres apuntados por pDataToSend hasta que encuentra el EOF_

void uartUsbSendCallback(void *unused)
{

	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	while (*pDataToSend != EOF_)
	{
		if (uartTxReady(UART_USB))
		{
			uartTxWrite(UART_USB, *pDataToSend++);
		}
		else
			break;
	}
	if (*pDataToSend == EOF_)
	{
		if (uartTxReady(UART_USB))
		{
			uartTxWrite(UART_USB, *pDataToSend);
			uartCallbackClr(UART_USB, UART_TRANSMITER_FREE);
			//aca se notifica a la tarea que se completó la transmisión y se desbloquea
			xTaskNotifyFromISR(xHandlePrintQueue, TX_BIT, eSetBits,
					&xHigherPriorityTaskWoken);
		}
	}
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/*
 * Imprime el paquete procesado en la terminal
 * Le agrega el SOF y el EOF
 *
 * NOTA: Como no se puede usar mutex en una función de atención a interrupción se usa una altenativa superadora a
 * los event groups llamada RTOS Task Notification. Es más eficiente, tiene menos overhead y ocupa menos memoria.
 *
 */
void Print_Queue(void* taskParmPtr)
{
	BaseType_t xResult;
	BaseType_t xStatus;
	buff_t txDatos;
	static uint8_t tamanio = 0;
	const TickType_t xMaxBlockTime = pdMS_TO_TICKS(200);
	while (TRUE)
	{
		xStatus = xQueueReceive(queueTransmitUart, &txDatos, portMAX_DELAY);
		if (xStatus == pdPASS)
		{

			pDataToSend = &DataToSend[0];
			DataToSend[0] = SOF_;
			memcpy(&DataToSend[1], &txDatos.buff[0], txDatos.largo);
			DataToSend[txDatos.largo + 1] = EOF_;
			uartCallbackSet(UART_USB, UART_TRANSMITER_FREE, uartUsbSendCallback,
			NULL);
			uartSetPendingInterrupt(UART_USB);
			//aca se bloquea esperando la notificación proveniente de uartUsbSendCallback indicando que
			//se terminó de transmitir el paquete por la uart
			xResult = xTaskNotifyWait(0, 0xffffffff, NULL, xMaxBlockTime);
		}
	}
}

/*==================[definiciones de funciones externas]=====================*/

/*==================[fin del archivo]========================================*/
