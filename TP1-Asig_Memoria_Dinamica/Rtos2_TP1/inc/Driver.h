/*================================[driver.h]==========================================================

 * Copyright 2019 Agustín Rey <agustinrey61@gmail.com> - Yoel López <lopez.yoel25@gmail.com>
 * All rights reserved.
 * Version: 1.0.0
 * Fecha de creacion: 2019/07/15

 */

/*=====[Evitar inclusion multiple comienzo]===========================================================*/

#ifndef _DRIVER_H_
#define _DRIVER_H_

/*=====[Inclusiones de dependencias de funciones publicas]===================*/

#include "sapi.h"
#include "service.h"


/*=====[C++ comienzo]========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=================[Macros de definición de constantes publicas]=========================*/

#define SOF_ '{'
#define EOF_ '}'

#define OP_ORDER 	0	/* orden del byte donde se encuentra el codigo de la operación*/
#define TAM_ORDER	1	/* orden del byte donde se encuentra el tamaño del paquete de datos*/
#define INI_ORDER	2	/* orden del byte donde se encuentra el primer elemento del paquete de datos*/

#define MIN_COD_OP     '0'	//valor máximo que puede tomar el código de la operación
#define MAX_COD_OP     '5'	//valor máximo que puede tomar el código de la operación
#define LONG_MAX_BYTES 99	//máxima longitud que puede tener el payload del paquet de datos
#define VX_BAUDIOS     115200	//baudrat


/*=================[Tipos de Datos]======================================*/

typedef struct{
	uint8_t largo;
	uint8_t buff[104];
}buff_t;


/*=================[Macros estilo funcion publicas]======================================*/

/* Definición de estados de la FSM de recepción por la UART */

typedef enum {
	WAIT_SOF,
	WAIT_EOF,
} rxStatus_t;

typedef enum {
	WAITING,
	TXSOF,
	TXDATA,
	TXEOF,
} txStatus_t;


/*=================[Prototipos de funciones privadas]====================================*/

/*Tarea por interrupción que recibe el paquete desde la consola*/
static void uartUsbSendCallback  (void *unused);

/*=================[Prototipos de funciones publicas]====================================*/

/*Inicializa la UART2 (USB) y luego queda esperando un paquete para enviarlo al servicio*/
void Initialize_UART             (void* taskParmPtr);
/*Imprime el paquete por consola recibido por la cola hasta que encuentra el EOF_*/
void Print_Queue                 (void* taskParmPtr);

/*=================[Prototipos de funciones publicas de interrupcion]====================*/

/*=====[C++ fin]=============================================================*/

#ifdef __cplusplus
}
#endif

/*=================[Evitar inclusion multiple fin]=======================================*/

#endif /* _DRIVER_H_ */
