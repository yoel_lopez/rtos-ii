/*
 * leds.h
 *
 *  Created on: 22 ago. 2019
 *      Author: agus
 */

#ifndef EXAMPLES_RTOS_II_TP3_POO_INC_LEDS_H_
#define EXAMPLES_RTOS_II_TP3_POO_INC_LEDS_H_

#include "FrameworkEventos.h"

typedef struct  {
	gpioMap_t Led;
	bool_t Estado;
} Led_t;


void DriverLeds(Evento_t *evn);
void DriverLedsInit(Modulo_t * pModulo);



#endif /* EXAMPLES_RTOS_II_TP3_POO_INC_LEDS_H_ */
