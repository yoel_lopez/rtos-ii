/*
 ================================[servicio.h]===================================================

 * Copyright 2019 Agustín Rey <agustinrey61@gmail.com> - Yoel López <lopez.yoel25@gmail.com>
 * All rights reserved.
 * Version: 1.0.0
 * Fecha de creacion: 2019/07/15 */

/*=====================[Evitar inclusion multiple comienzo]==================================*/

#ifndef _SERVICIO_H_
#define _SERVICIO_H_


/*=====================[Inclusiones de dependencias de funciones publicas]===================*/

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "semphr.h"
#include "stdio.h"
#include "string.h"
#include "task.h"
#include "Driver.h"


/*====================[C++ comienzo]========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[Macros de definicion de constantes publicas]=========================*/

/*==================[Macros estilo funcion publicas]======================================*/

/*==================[Definiciones de datos internos]=====================================*/
#define LEN 104 //99 datos+eof+sof+op+tamaño(2 bytes)

typedef enum {
	UPPERCASE = '0', LOWERCASE = '1', STACK_REPORT = '2', HEAP_REPORT = '3', MEASURE_PERFORMANCE = '4', RESP_MEAS_PERFORMANCE= '5', REP_PERFORMANCE = '6' ,NO_OP = -1

} eOperation_t;


typedef struct {
	eOperation_t op;         /* Código identificador del tipo de operación */
	uint8_t tamanio;        /*  Tamanio del paquete*/
	char pvData[LEN]; /* Estructura con datos del paquete*/
} packet_t;


typedef struct {
	eOperation_t op;    /* Código identificador del tipo de operación */
	int tamanio;   /*  Tamanio del paquete*/
	char* pDatos;     /* Puntero a datos del paquete*/
} datos_t;

typedef struct{
	uint32_t id_de_paquete;
	uint16_t largo_paquete;
	uint16_t memoria_alojada;
	uint32_t tiempo_llegada;
	uint32_t tiempo_recepcion;
	uint32_t tiempo_inicio;
	uint32_t tiempo_fin;
	uint32_t tiempo_salida;
	uint32_t tiempo_transmision;
	datos_t *datos;
}token_t;




/*==================[Prototipos de funciones privadas]====================================*/

/*==================[Prototipos de funciones publicas]====================================*/

void Initialize_Service(void* taskParmPtr);
void Create_And_Assign_Queue(QueueHandle_t queueRecieveUart);

/*==================[Prototipos de funciones publicas de interrupcion]====================*/

/*==================[C++ fin]=============================================================*/

#ifdef __cplusplus
}
#endif

/*==================[Evitar inclusion multiple fin]=======================================*/

#endif /* _SERVICIO_H_ */
