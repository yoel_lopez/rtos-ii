/*
 * seniales.h
 *
 *  Created on: 15/06/2012
 *      Author: alejandro
 */

#ifndef SENIALES_H_
#define SENIALES_H_

typedef enum
{
    SIG_MODULO_INICIAR	= 0		,
    SIG_TIMEOUT					,
	SIG_LEDOFF					,
    SIG_LEDON					,
    SIG_PULSADOR_APRETADO		,
    SIG_PULSADOR_LIBERADO		,
    SIG_ULTIMO_VALOR = SIG_PULSADOR_LIBERADO
} Signal_t;

extern Modulo_t * ModuloBroadcast;
extern Modulo_t * ModuloLuzSensor;
extern Modulo_t * ModuloLuzControl;
extern Modulo_t * ModuloGasSensor;
extern Modulo_t * ModuloGasControl;
extern Modulo_t * ModuloPuertaControl;
extern Modulo_t * ModuloInformeEstado;

#endif /* SENIALES_H_ */
