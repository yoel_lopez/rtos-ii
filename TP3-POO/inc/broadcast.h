/*
 * broadcast.h
 *
 *  Created on: 29/11/2011
 *      Author: alejandro
 */

#ifndef BROADCAST_H_
#define BROADCAST_H_

#include "modulos.h"
#include "service.h"


void EncolaNotificacion		   (datos_t reporte, QueueHandle_t cola);
void ManejadorEventosBroadcast ( Evento_t * evn );

#endif /* BROADCAST_H_ */
