/*
 * evento.h
 *
 *  Created on: 11/05/2012
 *      Author: alejandro
 */

#ifndef EVENTOS_H_
#define EVENTOS_H_

#include "FrameworkEventos.h"
#include "seniales.h"

portBASE_TYPE EncolarEventoFromISR ( Modulo_t * receptor, Signal_t senial, int valor );

#endif /* EVENTO_H_ */
