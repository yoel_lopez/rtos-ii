/*
 * luz_sensor.h
 *
 *  Created on: 29/11/2011
 *      Author: alejandro
 */

#ifndef PUERTA_SENSOR_H_
#define PUERTA_SENSOR_H_

#include "FrameworkEventos.h"

typedef enum EstadosPuertaSensorEnum {
	sPULSADOR_IDLE = 0,
	sPULSADOR_ESPERANDO_PULSACION,
	sPULSADOR_CONFIRMANDO_PULSACION,
	sPULSADOR_ESPERANDO_LIBERACION,
	sPULSADOR_CONFIRMANDO_LIBERACION
} FsmPulsadorStatus_t;


typedef struct PulsadorStruct {
	gpioMap_t boton;
	FsmPulsadorStatus_t estadoFSM;
	gpioMap_t led;
	uint32_t tpulsado;
	uint32_t tsoltado;
	uint32_t duracion;
	int ticksFiltrados;
	int idPulsador;		//en este caso el idPulsador coincide con el Idled
} Pulsador;


void My_IRQ_Init (void);
void DriverPulsadores ( Evento_t *evn );

#endif /* LUZ_SENSOR_H_ */
