/*
 ================================[main.c]===================================================

 * Copyright 2019 Agustín Rey <agustinrey61@gmail.com> - Yoel López <lopez.yoel25@gmail.com>
 * All rights reserved.
 * Version: 1.0.0
 * Fecha de creacion: 2019/07/15

 */

/*==================================[inclusiones]============================================*/

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "sapi.h"

/*===========================[inclusiones - bibliotecas propias]============================================*/

#include "Driver.h"
#include "service.h"
#include "FrameworkEventos.h"
#include "pulsadores.h"
#include "leds.h"

/*================================[definiciones y macros]==================================*/

/*================================[definiciones de datos externos]=========================*/
extern xQueueHandle colaEventosPrioridadBaja;

/*================================[definiciones de datos globales]=========================*/

TaskHandle_t xHandlePrintQueue = NULL; //se usa para que la notificación de fin de transmision de parte de la interrupcion


Modulo_t * ModuloDriverPulsadores;
Modulo_t * ModuloBroadcast;
Modulo_t * ModuloLeds;
/*=============================[declaraciones de funciones internas]====================*/

/*=====================================[declaraciones de tareas]====================*/

/*========================================[Función principal]======================================*/

int main(void) {

	/* ------------- Configuraciones---------------------------------*/

	boardConfig();
	My_IRQ_Init();

	/* ------------- Acá declarar el heap inicial de la app---------------------------------*/

	/* ---------- Creación de tareas ------------------------------*/

	/* Esta tarea se encarga de inicializar la UART y las estructuras de las colas para envío y recepción de
	 * la información proveniente de las tareas restantes */

	/* Esta tarea se encarga de crear e inicializar las estructuras de las colas*/

	xTaskCreate(Initialize_Service, (const char *) "Initialize Service",
	configMINIMAL_STACK_SIZE * 2, 0, tskIDLE_PRIORITY + 1, 0);


	//Initialize_UART(0);
	xTaskCreate(Initialize_UART, (const char *) "Initialize UART",
	configMINIMAL_STACK_SIZE * 2, 0, tskIDLE_PRIORITY + 1, 0);


	/* Esta tarea se encarga de la lectura de los paquetes provenientes de las colas y aplica las operaciones
	 * mayusculizar y minusculizar */

	xTaskCreate(Create_And_Assign_Queue, (const char *) "Assign Queue",
	configMINIMAL_STACK_SIZE * 2, 0, tskIDLE_PRIORITY + 1, 0);

	/* Esta tarea se encarga de mostrar la información procesada por la UART*/

	xTaskCreate(Print_Queue, (const char *) "Print UART",
	configMINIMAL_STACK_SIZE * 2, 0, tskIDLE_PRIORITY + 1, &xHandlePrintQueue);

	colaEventosPrioridadBaja = xQueueCreate(5, sizeof(Evento_t));

	// Creo la tarea de baja prioridad
	xTaskCreate(TareaDespachadoraEventos, /* Pointer to the function that implements the task. */
	"Control", /* Text name for the task.  This is to facilitate debugging only. */
	5 * configMINIMAL_STACK_SIZE, /* Stack depth in words. */
	(void*) colaEventosPrioridadBaja, /* Parametro de la tarea */
	tskIDLE_PRIORITY, /* Prioridad */
	NULL); /* Task handle. */

	ModuloLeds = RegistrarModulo(DriverLeds,PRIORIDAD_BAJA);
	ModuloDriverPulsadores = RegistrarModulo(DriverPulsadores, PRIORIDAD_BAJA);
	ModuloBroadcast = RegistrarModulo(ManejadorEventosBroadcast,PRIORIDAD_BAJA);

	IniciarTodosLosModulos();


	/* Iniciar el scheduler*/

	vTaskStartScheduler();

	while ( TRUE) {
	}
	return 0;
}

/*==================[definiciones de funciones internas]=====================*/

/*==================[definiciones de funciones externas]=====================*/

/*==================[fin del archivo]========================================*/
