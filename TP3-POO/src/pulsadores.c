/*
 * luz_sensor.c
 *
 *  Created on: 29/11/2011
 *      Author: alejandro
 */

#include "FrameworkEventos.h"
#include "sapi.h"
#include "sapi_peripheral_map.h"
#include "seniales.h"
#include "leds.h"
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "pulsadores.h"


void FSM_Pulsador(Pulsador * pulsador, Evento_t * evn);

#define MAX_PULSADORES 4
Pulsador vectorPulsadores[MAX_PULSADORES];
extern Led_t vectorLeds[];

static int nPulsadores = 0;

static Modulo_t * mod;

void PulsadorInit(Pulsador * p, gpioMap_t Tecla) {
	p->idPulsador = nPulsadores;
	nPulsadores++;
	p->boton = Tecla;
	p->estadoFSM = sPULSADOR_ESPERANDO_PULSACION;
	p->ticksFiltrados = 0;
	p->led = vectorLeds[p->idPulsador].Led;
}

void DriverPulsadoresInit(Modulo_t * pModulo) {
	PulsadorInit(&vectorPulsadores[0], TEC1);
	PulsadorInit(&vectorPulsadores[1], TEC2);
	PulsadorInit(&vectorPulsadores[2], TEC3);
	PulsadorInit(&vectorPulsadores[3], TEC4);

	pModulo->periodo = 20;
}

void DriverPulsadores(Evento_t *evn) {
	int i;
	switch (evn->signal) {
	case SIG_MODULO_INICIAR:
		mod = (Modulo_t *) evn->valor;
		DriverPulsadoresInit(mod);
		TimerArmRepetitivo(mod, mod->periodo);
		break;

	case SIG_TIMEOUT:
		for (i = 0; i < nPulsadores; i++) {
			FSM_Pulsador(&vectorPulsadores[i], evn);
		}
		break;

	default:	// Ignoro todas las otras seniales
		break;
	}
}

void FSM_Pulsador(Pulsador * pulsador, Evento_t * evn) {
	Evento_t _evn;
	if (pulsador->ticksFiltrados > 0) {
		pulsador->ticksFiltrados--;
		return;
	}

	switch (pulsador->estadoFSM) {
	//-----------------------------------------------------------------------------
	case sPULSADOR_ESPERANDO_PULSACION:
		if (gpioRead(pulsador->boton) == 0) {
			pulsador->estadoFSM = sPULSADOR_CONFIRMANDO_PULSACION;
			pulsador->ticksFiltrados = 0;
		} else {
			pulsador->ticksFiltrados = 5;
		}
		break;
		//-----------------------------------------------------------------------------
	case sPULSADOR_CONFIRMANDO_PULSACION:
		if ((gpioRead(pulsador->boton) == 0)) {
			pulsador->estadoFSM = sPULSADOR_ESPERANDO_LIBERACION;
			pulsador->ticksFiltrados = 5;
			EncolarEvento(ModuloBroadcast, SIG_PULSADOR_APRETADO,
					pulsador->idPulsador);
			EncolarEvento(ModuloBroadcast, SIG_LEDON, pulsador->led);
		} else {
			pulsador->estadoFSM = sPULSADOR_ESPERANDO_PULSACION;
			pulsador->ticksFiltrados = 0;
		}
		break;
		//-----------------------------------------------------------------------------
	case sPULSADOR_ESPERANDO_LIBERACION:
		if ((gpioRead(pulsador->boton) != 0)) {
			pulsador->estadoFSM = sPULSADOR_CONFIRMANDO_LIBERACION;
			pulsador->ticksFiltrados = 0;
		} else {
			pulsador->ticksFiltrados = 5;
		}
		break;
		//-----------------------------------------------------------------------------
	case sPULSADOR_CONFIRMANDO_LIBERACION:
		if ((gpioRead(pulsador->boton) != 0)) {
			//Liberacion confirmada, encolo un evento
			pulsador->estadoFSM = sPULSADOR_ESPERANDO_PULSACION;
			pulsador->ticksFiltrados = 5;
			EncolarEvento(ModuloBroadcast, SIG_PULSADOR_LIBERADO,
					pulsador->idPulsador);
			EncolarEvento(ModuloBroadcast, SIG_LEDOFF, pulsador->led);
		}
		break;
		//-----------------------------------------------------------------------------
	default:
		// Se me fue de valor la variable de estado!
		// TODO: Reinicializar ESTE pulsador
		break;
	}
}

//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------

#include "board.h"

#define UP      1
#define FALLING 2
#define DOWN	3
#define RISING  4

#define CANT_TECLAS 4
#define CANT_LEDS 4
#define ANTI_BOUNCE_TIME_MS 20

enum Teclas_t {
	Tecla1, Tecla2, Tecla3, Tecla4
};
//Índices de teclas para el vector de estructuras

struct Button_Control { //estructura de control de datos capturados por la interrupción
	portTickType TiempoCaptura;
	uint8_t Flanco;
};

struct Buttons_SM_t { //estructura de control de la máquina de estados de cada botón
	uint8_t Indice;
	uint8_t Estado;
	xQueueHandle Cola;
	portTickType TiempoIntermedio;
};

struct Lectura_t {
	uint8_t Indice;
	portTickType TiempoMedido;
};

xQueueHandle Cola_Lecturas;

//Definición de vector de estructuras de control
struct Buttons_SM_t Buttons_SM[CANT_TECLAS];

/*==================[definiciones de funciones internas]=====================*/
//Función de inicialización de IRQs
void My_IRQ_Init(void) {
	//Inicializamos las interrupciones (LPCopen)
	Chip_PININT_Init(LPC_GPIO_PIN_INT);

	//Inicializamos de cada evento de interrupción (LPCopen)
	Chip_SCU_GPIOIntPinSel(0, 0, 4); //Mapeo del pin donde ocurrirá el evento y
									 //el canal al que lo va a enviar. (Canal 0 a 7, Puerto GPIO, Pin GPIO)
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH0);//Se configura el canal
															//para que se active por flanco
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH0);//Se configura para que el
														  //flanco sea el de bajada

	Chip_SCU_GPIOIntPinSel(1, 0, 4);//En este caso el canal de interrupción es 1
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH1);
	Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH1);	//En este caso el flanco es
															//de subida

	Chip_SCU_GPIOIntPinSel(2, 0, 8);
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH2);
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH2);

	Chip_SCU_GPIOIntPinSel(3, 0, 8);
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH3);
	Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH3);

	Chip_SCU_GPIOIntPinSel(4, 0, 9);
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH4);
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH4);

	Chip_SCU_GPIOIntPinSel(5, 0, 9);
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH5);
	Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH5);

	Chip_SCU_GPIOIntPinSel(6, 1, 9);
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH6);
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH6);

	Chip_SCU_GPIOIntPinSel(7, 1, 9);
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH7);
	Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH7);

	//Una vez que se han configurado los eventos para cada canal de interrupcion
	//Se activan las interrupciones para que comiencen a llamar al handler
	NVIC_SetPriority(PIN_INT0_IRQn, 2);
	NVIC_EnableIRQ(PIN_INT0_IRQn);
	NVIC_SetPriority(PIN_INT1_IRQn, 2);
	NVIC_EnableIRQ(PIN_INT1_IRQn);
	NVIC_SetPriority(PIN_INT2_IRQn, 2);
	NVIC_EnableIRQ(PIN_INT2_IRQn);
	NVIC_SetPriority(PIN_INT3_IRQn, 2);
	NVIC_EnableIRQ(PIN_INT3_IRQn);
	NVIC_SetPriority(PIN_INT4_IRQn, 2);
	NVIC_EnableIRQ(PIN_INT4_IRQn);
	NVIC_SetPriority(PIN_INT5_IRQn, 2);
	NVIC_EnableIRQ(PIN_INT5_IRQn);
	NVIC_SetPriority(PIN_INT6_IRQn, 2);
	NVIC_EnableIRQ(PIN_INT6_IRQn);
	NVIC_SetPriority(PIN_INT7_IRQn, 2);
	NVIC_EnableIRQ(PIN_INT7_IRQn);

}

void GPIO0_IRQHandler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE; //Comenzamos definiendo la variable

	if (Chip_PININT_GetFallStates(LPC_GPIO_PIN_INT) & PININTCH0) { //Verificamos que la interrupción es la esperada
		Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH0); //Borramos el flag de interrupción
		//codigo a ejecutar si ocurrió la interrupción
		vectorPulsadores[0].tpulsado = xTaskGetTickCountFromISR();
	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void GPIO1_IRQHandler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (Chip_PININT_GetRiseStates(LPC_GPIO_PIN_INT) & PININTCH1) {
		Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH1);
		//codigo a ejecutar si ocurrió la interrupción
		vectorPulsadores[0].tsoltado = xTaskGetTickCountFromISR();
		vectorPulsadores[0].duracion = vectorPulsadores[0].tsoltado - vectorPulsadores[0].tpulsado;

	}
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void GPIO2_IRQHandler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE; //Comenzamos definiendo la variable

	if (Chip_PININT_GetFallStates(LPC_GPIO_PIN_INT) & PININTCH2) { //Verificamos que la interrupción es la esperada
		Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH2); //Borramos el flag de interrupción
		//codigo a ejecutar si ocurrió la interrupción
		vectorPulsadores[1].tpulsado = xTaskGetTickCountFromISR();
	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void GPIO3_IRQHandler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (Chip_PININT_GetRiseStates(LPC_GPIO_PIN_INT) & PININTCH3) {
		Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH3);
		//codigo a ejecutar si ocurrió la interrupción
		vectorPulsadores[1].tsoltado = xTaskGetTickCountFromISR();
		vectorPulsadores[1].duracion = vectorPulsadores[1].tsoltado - vectorPulsadores[1].tpulsado;
	}
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void GPIO4_IRQHandler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE; //Comenzamos definiendo la variable

	if (Chip_PININT_GetFallStates(LPC_GPIO_PIN_INT) & PININTCH4) { //Verificamos que la interrupción es la esperada
		Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH4); //Borramos el flag de interrupción
		//codigo a ejecutar si ocurrió la interrupción
		vectorPulsadores[2].tpulsado = xTaskGetTickCountFromISR();
	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void GPIO5_IRQHandler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (Chip_PININT_GetRiseStates(LPC_GPIO_PIN_INT) & PININTCH5) {
		Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH5);
		//codigo a ejecutar si ocurrió la interrupción
		vectorPulsadores[2].tsoltado = xTaskGetTickCountFromISR();
		vectorPulsadores[2].duracion = vectorPulsadores[2].tsoltado - vectorPulsadores[2].tpulsado;
	}
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void GPIO6_IRQHandler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE; //Comenzamos definiendo la variable

	if (Chip_PININT_GetFallStates(LPC_GPIO_PIN_INT) & PININTCH6) { //Verificamos que la interrupción es la esperada
		Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH6); //Borramos el flag de interrupción
		//codigo a ejecutar si ocurrió la interrupción
		vectorPulsadores[3].tpulsado = xTaskGetTickCountFromISR();
	}

	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void GPIO7_IRQHandler(void) {
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;

	if (Chip_PININT_GetRiseStates(LPC_GPIO_PIN_INT) & PININTCH7) {
		Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH7);
		//codigo a ejecutar si ocurrió la interrupción
		vectorPulsadores[3].tsoltado = xTaskGetTickCountFromISR();
		vectorPulsadores[3].duracion = vectorPulsadores[3].tsoltado - vectorPulsadores[3].tpulsado;
	}
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

