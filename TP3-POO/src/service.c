/*
 ================================[servicio.c]===================================================

 * Copyright 2019 Agustín Rey <agustinrey61@gmail.com> - Yoel López <lopez.yoel25@gmail.com>
 * All rights reserved.
 * Version: 1.0.0
 * Fecha de creacion: 2019/07/15

 */

/*==================[inclusiones]============================================*/
#include "stdlib.h"
#include "ctype.h"

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "sapi.h"
/*==================[inclusiones - bibliotecas propias]============================================*/

#include "service.h"
#include "Driver.h"

/*==================[definiciones y macros]==================================*/

/*==================[definiciones de datos internos]=========================*/

extern infoDriverTx_t infoDriverTx;
char bufferTransmitir[LEN];
packet_t packet;

/*==================[definiciones de datos externos]=========================*/

QueueHandle_t queueMayusculizar, queueMinusculizar, queueMedirPerformance; /* Colas para procesamiento y envío de resultados*/

QueueHandle_t queueRecieveUart, queueTransmitUart; /* Colas para recepción y envío por UART */

SemaphoreHandle_t xSemaphoreFinMedicion;
/*==================[declaraciones de funciones internas]=====================*/
static void cargarStructTx(buff_t *buff, datos_t *ReceivedStructure);
static void cargarRemainingHeap(buff_t *pbuff);
static void cargarRemainingStack(buff_t *pbuff);
static void EncolaReporte(datos_t reporte, QueueHandle_t cola);
/*==================[declaraciones de sub-tareas]================================*/

void write_Package_ToQueueMayus(void* taskParmPtr);
void read_process_Package_FromQueueMayus(void* taskParmPtr);
void write_Package_ToQueueMinus(void* taskParmPtr);
void read_process_Package_FromQueueMinus(void* taskParmPtr);

/*==================[definiciones de funciones internas]=====================*/
char repBuff[150];
void process_Package_FromQueuePerformance(void* taskParmPtr) {

	token_t *xReceivedStructure; /* Se declara la estructura que contiene los valores a leer desde la cola. */
	BaseType_t xStatus;
	uint8_t i;
	char *ptr;
	buff_t buff;
	datos_t reporte;

	for (;;) {
		xStatus = xQueueReceive(queueMedirPerformance, &xReceivedStructure,
		portMAX_DELAY); //se queda esperando a recibir datos de la queue
		if (xStatus == pdPASS) {
			if (xReceivedStructure->datos->op == MEASURE_PERFORMANCE) { //verifica que todo esta OK
				ptr = xReceivedStructure->datos->pDatos; //si es asi, convierte a mayúsculas

				xReceivedStructure->tiempo_inicio = xTaskGetTickCount(); //registra cuando se extrae el puntero a los datos a procesar

				for (i = 0; i < xReceivedStructure->datos->tamanio; i++) {
					*ptr = toupper(*ptr); /* Función que convierte los datos a mayúsculas*/
					ptr++;
				}
				/* Los datos convertidos se envian a la cola TransmitUart*/
				/* pero primero cargamos los datos en una estructura tipo buff_t*/
				xReceivedStructure->datos->op=RESP_MEAS_PERFORMANCE;
				cargarStructTx(&buff, xReceivedStructure->datos);

				if (xQueueSend(queueTransmitUart, &buff,
						1) != pdTRUE) {
					printf("error al enviar por cola queueTransmitUart");
				}
				xReceivedStructure->tiempo_fin = xTaskGetTickCount(); //registra cuando se envían los datos por la cola de transmision

				//se queda esperando fin de transmision por parte del driver
				xSemaphoreTake(xSemaphoreFinMedicion,1);
				if ( xSemaphoreTake(xSemaphoreFinMedicion,portMAX_DELAY) == pdTRUE) {
					xReceivedStructure->tiempo_salida =	infoDriverTx.tiempo_emision_STX;
					xReceivedStructure->tiempo_transmision = infoDriverTx.tiempo_emision_ETX;

					 reporte.op = REP_PERFORMANCE;
					 reporte.pDatos = repBuff;
					 sprintf(repBuff,"Tiempos: llega=%d, recep=%d, ini=%d, fin=%d, sal=%d, trans=%d",
					 xReceivedStructure->tiempo_llegada, xReceivedStructure->tiempo_recepcion,
					 xReceivedStructure->tiempo_inicio,xReceivedStructure->tiempo_fin,
					 xReceivedStructure->tiempo_salida,xReceivedStructure->tiempo_transmision);
					 reporte.tamanio = strlen(repBuff);
					 EncolaReporte(reporte, queueTransmitUart);
				}
			}
		} else {
			printf("No se leyeron datos de la cola.\r\n");
		}

		vPortFree(xReceivedStructure->datos);
		vPortFree(xReceivedStructure);
		vTaskDelete(xTaskGetCurrentTaskHandle());
	}

}
/*===================== Tipo de Operación Mayusculizar =====================*/

void write_Package_ToQueueMayus(void* taskParmPtr)

{
	BaseType_t xStatus;
	const TickType_t xTicksToWait = pdMS_TO_TICKS(100);

	for (;;)

	{
		xStatus = xQueueSendToBack(queueMayusculizar, taskParmPtr,
				portMAX_DELAY);

		if (xStatus != pdPASS) {
			printf(" No se escribieron datos en la cola mayusculizar.\r\n");
		}
		vTaskDelete(xTaskGetCurrentTaskHandle());
	}

}
void read_process_Package_FromQueueMayus(void* taskParmPtr)

{
	datos_t xReceivedStructure; /* Se declara la estructura que contiene los valores a leer desde la cola. */
	BaseType_t xStatus;
	uint8_t i;
	char *ptr;
	buff_t buff;

	for (;;) {
		xStatus = xQueueReceive(queueMayusculizar, &xReceivedStructure,
		portMAX_DELAY);

		if (xStatus == pdPASS)

		{
			if (xReceivedStructure.op == UPPERCASE) {
				ptr = xReceivedStructure.pDatos;
				for (i = 0; i < xReceivedStructure.tamanio; i++) {
					*ptr = toupper(*ptr); /* Función que convierte los datos a mayúsculas*/
					ptr++;
				}

				/* Los datos convertidos se envian a la cola TransmitUart*/
				/* pero primero cargamos los datos en una estructura tipo buff_t*/
				cargarStructTx(&buff, &xReceivedStructure);

				if (xQueueSend(queueTransmitUart, &buff,
						1) != pdTRUE) {
					printf("error al enviar por cola queueRecieveUart");
				}

				/* Informa por la misma cola el stack remanente*/

				cargarRemainingStack(&buff);
				if (xQueueSend(queueTransmitUart, &buff,
						1) != pdTRUE) {
					printf("error al enviar por cola queueRecieveUart");
				}

			}

		} else {
			printf("No se leyeron datos de la cola.\r\n");
		}
		/* Libera la memoria y finaliza la tarea.*/

		vPortFree(xReceivedStructure.pDatos);
		vTaskDelete(xTaskGetCurrentTaskHandle());
	}
}

/*===================== Tipo de Operación Minusculizar =====================*/

void write_Package_ToQueueMinus(void* taskParmPtr)

{
	BaseType_t xStatus;
	const TickType_t xTicksToWait = pdMS_TO_TICKS(100);

	for (;;)

	{
		xStatus = xQueueSendToBack(queueMinusculizar, taskParmPtr,
				portMAX_DELAY);

		if (xStatus != pdPASS) {
			printf(" No se escribieron datos en la cola minusculizar.\r\n");
		}
		vTaskDelete(xTaskGetCurrentTaskHandle());
	}

}
void read_process_Package_FromQueueMinus(void* taskParmPtr)

{
	datos_t xReceivedStructure; /* Se declara la estructura que contiene los valores a leer desde la cola. */
	BaseType_t xStatus;
	uint8_t i;
	char *ptr;
	buff_t buff;
	char xstr[10];
	size_t remainingHeap;

	for (;;) {
		xStatus = xQueueReceive(queueMinusculizar, &xReceivedStructure,
		portMAX_DELAY);

		if (xStatus == pdPASS) {
			if (xReceivedStructure.op == LOWERCASE) {
				ptr = xReceivedStructure.pDatos;

				for (i = 0; i < xReceivedStructure.tamanio; i++) {
					*ptr = tolower(*ptr); /* Función que convierte los datos a minúsculas*/
					ptr++;
				}

				/* Los datos convertidos se envian a la cola TransmitUart*/
				/* pero primero cargamos los datos en una estructura tipo buff_t*/

				cargarStructTx(&buff, &xReceivedStructure);

				if (xQueueSend(queueTransmitUart, &buff,
						1) != pdTRUE) {
					printf("error al enviar por cola queueRecieveUart");
				}

				/* Informa por la misma cola el stack remanente*/

				cargarRemainingStack(&buff);
				if (xQueueSend(queueTransmitUart, &buff,
						1) != pdTRUE) {
					printf("error al enviar por cola queueRecieveUart");
				}

			} else {
				printf("No se leyeron datos de la cola.\r\n");
			}
		}

		/* Elimina la cola que ya se usó, libera la memoria y finaliza la tarea.*/

		vPortFree(xReceivedStructure.pDatos);
		vTaskDelete(xTaskGetCurrentTaskHandle());
	}
}

/*==================[definiciones de funciones externas]=====================*/

/*Esta tarea inicializa las colas Mayusculizar, Minusculizar y Transmitir
 *
 * Esta tarea es "One Shot" y se suicida
 */

void Initialize_Service(void* taskParmPtr) {

	xSemaphoreFinMedicion = xSemaphoreCreateBinary();

	if ((queueMayusculizar = xQueueCreate(1, sizeof(datos_t))) == NULL) {
		printf("error al crear la queue Mayusculizar");
	}

	if ((queueMinusculizar = xQueueCreate(1, sizeof(datos_t))) == NULL) {
		printf("error al crear la queue Minusculizar");
	}

	if ((queueMedirPerformance = xQueueCreate(1, sizeof(token_t *))) == NULL) {
		printf("error al crear la queue Transmitir");
	}

	vTaskDelete(xTaskGetCurrentTaskHandle());
}

volatile static infoDriverRx_t *xpinfo;

#define BUFF_LEN 50
void Create_And_Assign_Queue(void* taskParmPtr) {
	BaseType_t xStatus;
	static datos_t Datos;
	static token_t *token;

	uint32_t remaining_heap;
	Datos.pDatos = NULL;
	buff_t rx_uart;
	buff_t buff;
	uint8_t length;
	uint8_t xstr[3];
	TaskHandle_t xHandle;
	TaskStatus_t xTaskDetails;
	infoDriverRx_t infoDriverRx;
	static uint32_t paqueteId = 0;

	cargarRemainingHeap(&buff);
	if (xQueueSend(queueTransmitUart, &buff,
			1) != pdTRUE) {
		printf("error al enviar por cola queueTransmitUart");
	}

	for (;;) {
		/* Escribir la cola con el paquete recibido en el buffer */

		xStatus = xQueueReceive(queueRecieveUart, &rx_uart, portMAX_DELAY); //

		if (xStatus == pdPASS) {

			Datos.op = rx_uart.buff[0];	//carga Datos con el código de operación
			xstr[0] = rx_uart.buff[1];
			xstr[1] = rx_uart.buff[2];
			xstr[3] = '\0';
			Datos.tamanio = atoi(&xstr[0]);	//carga Datos con la longitud en formato entero

			//hace un checkeo de que el tamaño y el código de operación sean correctos
			if ((Datos.tamanio == rx_uart.largo - 3)
					&& (Datos.op <= MAX_COD_OP)) {
				/* Se aloca la memoria dinámica necesaria para el manejo del paquete recibido */
				if ((Datos.pDatos = pvPortMalloc(Datos.tamanio)) == NULL)
					printf("error al ejecutar el pvPortMalloc");
				else {
					memcpy(Datos.pDatos, &rx_uart.buff[3], Datos.tamanio);

					switch (Datos.op)
					/* Se ejecutan las tareas según el tipo de operación */
					{

					case MEASURE_PERFORMANCE:
						if (queueMedirPerformance != NULL) {

							memcpy(&infoDriverRx,
									&rx_uart.buff[Datos.tamanio + 3],
									sizeof(infoDriverRx));//Se recuperan tiempos de recepción

							if ((token = pvPortMalloc(sizeof(token_t))) == NULL) {//se aloca memoria para el token
								printf("Error pvPortMalloc");
								break;
							} else {	//se llenan algunos datos en el token
								token->datos = &Datos;
								token->id_de_paquete = paqueteId++;
								token->largo_paquete = Datos.tamanio;
								token->memoria_alojada = Datos.tamanio;
								token->tiempo_llegada =
										infoDriverRx.tiempo_recepcion_STX;
								token->tiempo_recepcion =
										infoDriverRx.tiempo_recepcion_ETX;
							}

							xStatus = xQueueSendToBack(queueMedirPerformance,
									&token, portMAX_DELAY);

							if (xStatus != pdPASS) {
								printf(
										" No se escribieron datos en la cola minusculizar.\r\n");
							}

							/* Se procesan los datos en la tarea*/

							xTaskCreate(process_Package_FromQueuePerformance,
									(const char *) "process_FromQueue_Performance",
									configMINIMAL_STACK_SIZE * 2,
									(void *) &token,
									tskIDLE_PRIORITY + 1, 0);
						}
						break;

					case UPPERCASE:

						if (queueMayusculizar != NULL)

						{

							/* Se escriben los datos recibidos en la cola Mayusculizar */

							xTaskCreate(write_Package_ToQueueMayus,
									(const char *) "write_Package_ToQueue_Mayus",
									configMINIMAL_STACK_SIZE * 2,
									(void *) &Datos,
									tskIDLE_PRIORITY + 1, 0);

							/* Se leen los datos de la cola Mayusculizar, se pasa el texto a mayúsculas y se escribe en la cola
							 * TransmitUart  */

							xTaskCreate(read_process_Package_FromQueueMayus,
									(const char *) "read_process_Package_FromQueue_Mayus",
									configMINIMAL_STACK_SIZE * 2,
									(void *) &Datos,
									tskIDLE_PRIORITY + 1, 0);
						}

						break;

					case LOWERCASE:

						if (queueMinusculizar != NULL)

						{
							/* Se escriben los datos recibidos en la cola Minusculizar */

							xTaskCreate(write_Package_ToQueueMinus,
									(const char *) "write_Package_ToQueue_Minus",
									configMINIMAL_STACK_SIZE * 2,
									(void *) &Datos,
									tskIDLE_PRIORITY + 1, 0);

							/* Se leen los datos de la cola Minusculizar, se pasa el texto a minúsculas y se escribe en la cola
							 * TransmitUart  */

							xTaskCreate(read_process_Package_FromQueueMinus,
									(const char *) "read_process_Package_FromQueue_Minus",
									configMINIMAL_STACK_SIZE * 2,
									(void *) &Datos,
									tskIDLE_PRIORITY + 1, 0);
						}

						break;
					default:
						break;
					}
				}
			}
		}
	}
}

/* Estas funciones llenan la estructura buff_t con los datos que se pasarán al driver para que los
 * transmita por la UART con el formato especificado*/

static void cargarStructTx(buff_t *buff, datos_t *ReceivedStructure) {
	uint8_t myStr[5];
	buff->largo = ReceivedStructure->tamanio + 3;
	buff->buff[0] = ReceivedStructure->op;
	sprintf(&myStr[0], "%2u", ReceivedStructure->tamanio);

	if (myStr[0] == ' ')
		buff->buff[1] = '0';
	else
		buff->buff[1] = myStr[0];
	buff->buff[2] = myStr[1];
	memcpy(&buff->buff[3], ReceivedStructure->pDatos, buff->largo);
}

static void cargarRemainingHeap(buff_t *pbuff) {
	size_t remainingHeap;
	char xstr[10];
	remainingHeap = xPortGetFreeHeapSize();
	pbuff->largo = sprintf(&pbuff->buff[3], "Remainig Heap= %d", remainingHeap)
			+ 3;
	pbuff->buff[0] = '3';
	sprintf(&xstr[0], "%2u", pbuff->largo);
	if (xstr[0] == ' ')
		pbuff->buff[1] = '0';
	else
		pbuff->buff[1] = xstr[0];
	pbuff->buff[2] = xstr[1];

}
static void cargarRemainingStack(buff_t *pbuff) {
	size_t remainingHeap;
	char xstr[10];
	uint16_t xi;
	TaskStatus_t xTaskDetails;
	vTaskGetInfo(xTaskGetCurrentTaskHandle(), &xTaskDetails, pdTRUE, eInvalid);

	xi = xTaskDetails.usStackHighWaterMark;
	pbuff->largo = sprintf(&pbuff->buff[3], "Unused Stack = %d", xi) + 3;
	pbuff->buff[0] = '3';
	sprintf(&xstr[0], "%2u", pbuff->largo);
	if (xstr[0] == ' ')
		pbuff->buff[1] = '0';
	else
		pbuff->buff[1] = xstr[0];
	pbuff->buff[2] = xstr[1];

}

static buff_t buffEncRep;

void EncolaReporte(datos_t reporte, QueueHandle_t cola) {
	uint8_t myStr[5];

	buffEncRep.largo = reporte.tamanio + 3;
	buffEncRep.buff[0] = reporte.op;
	sprintf(&myStr[0], "%2u", reporte.tamanio);

	if (myStr[0] == ' ')
		buffEncRep.buff[1] = '0';
	else
		buffEncRep.buff[1] = myStr[0];
	buffEncRep.buff[2] = myStr[1];
	memcpy(&buffEncRep.buff[3], reporte.pDatos, buffEncRep.largo);

	if (xQueueSend(cola, &buffEncRep, 1) != pdTRUE) {
		printf("error al enviar por cola queueTransmitUart");
	}
}


/*===============================[fin del archivo]========================================*/

