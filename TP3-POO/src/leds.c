/*
 * leds.c
 *
 *  Created on: 22 ago. 2019
 *      Author: agus
 */

#include "leds.h"
#include "sapi.h"



#define MAX_LEDS 4
Led_t vectorLeds[MAX_LEDS];
static Modulo_t * mod;


void LedInit(Led_t * pled, gpioMap_t led) {
	pled->Led=led;
	pled->Estado=FALSE;
	gpioWrite( pled->Led, pled->Estado);
}

void DriverLedsInit(Modulo_t * pModulo) {
	LedInit(&vectorLeds[0], LEDB);
	LedInit(&vectorLeds[1], LED1);
	LedInit(&vectorLeds[2], LED2);
	LedInit(&vectorLeds[3], LED3);
}

void LedOn(Evento_t *evn){
	gpioWrite(vectorLeds[evn->valor].Led,TRUE);
}
void LedOff(Evento_t *evn){
	gpioWrite(vectorLeds[evn->valor].Led,FALSE);
}


void DriverLeds(Evento_t *evn) {
	int i;
	switch (evn->signal) {
	case SIG_MODULO_INICIAR:
		mod = (Modulo_t *) evn->valor;
		DriverLedsInit(mod);
		break;

	case SIG_LEDON:
		//mod = (Modulo_t *) evn->valor;
		LedOn(evn);
		break;

	case SIG_LEDOFF:
		//mod = (Modulo_t *) evn->valor;
		LedOff(evn);
		break;

		break;

	default:	// Ignoro todas las otras seniales
		break;
	}
}
