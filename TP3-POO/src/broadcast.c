/*
 * broadcast.c
 *
 *  Created on: 29/11/2011
 *      Author: alejandro
 */

#include "FrameworkEventos.h"
#include "pulsadores.h"

extern Pulsador vectorPulsadores[];

typedef enum estadosBroadcastEnum {
	sBROADCAST_IDLE = 0, sBROADCAST_NORMAL
} EstadosBroadcast_t;

//#include "UART.h"
//extern UART consola;
extern QueueHandle_t queueTransmitUart;
static int estadoBroadcast = sBROADCAST_IDLE;
const char pulsadorApretado[] = "Pulsador Apretado:";
const char pulsadorLiberado[] = "Pulsador Liberado:";
const char tiempoPulsado[] = "Tiempo de Pulsado =";

void EncolaNotificacion(datos_t reporte, QueueHandle_t cola) {
	uint8_t myStr[5];
	buff_t buff;
	buff.largo = reporte.tamanio + 3;
	buff.buff[0] = reporte.op;
	sprintf(&myStr[0], "%2u", reporte.tamanio);

	if (myStr[0] == ' ')
		buff.buff[1] = '0';
	else
		buff.buff[1] = myStr[0];
	buff.buff[2] = myStr[1];
	memcpy(&buff.buff[3], reporte.pDatos, buff.largo);

	if (xQueueSend(cola, &buff, 1) != pdTRUE) {
		printf("error al enviar por cola queueRecieveUart");
	}
}

void ManejadorEventosBroadcast(Evento_t * evn) {

	char buf[100];
	datos_t datos;

	switch (estadoBroadcast) {
	//-----------------------------------------------------------------------------
	case sBROADCAST_IDLE:
		switch (evn->signal) {
		case SIG_MODULO_INICIAR:
			estadoBroadcast = sBROADCAST_NORMAL;
			break;
		default:
			break;
		}
		break;
		//-----------------------------------------------------------------------------
	case sBROADCAST_NORMAL:
		switch (evn->signal) {

		case SIG_PULSADOR_APRETADO:
			datos.op = '6';
			datos.pDatos = buf;
			if (sprintf(buf, "%s %d", pulsadorApretado, evn->valor) > 0) {
				datos.tamanio = strlen(buf);
				EncolaNotificacion(datos, queueTransmitUart);
			} else
				printf("Error en sprintf\r\n");

			//ReenviarEvento	( ModuloPuertaControl, evn );
			break;

		case SIG_PULSADOR_LIBERADO:
			datos.op = '6';
			datos.pDatos = buf;
			if (sprintf(buf, "%s %d - %s %d", pulsadorLiberado, evn->valor, tiempoPulsado, vectorPulsadores[evn->valor].duracion ) > 0) {
				datos.tamanio = strlen(buf);
				EncolaNotificacion(datos, queueTransmitUart);
			} else
				printf("Error en sprintf\r\n");
			break;

		case SIG_LEDOFF:
			gpioWrite(evn->valor, FALSE);
			break;

		case SIG_LEDON:
			gpioWrite(evn->valor, TRUE);
			break;

		default:
			break;
		}
		break;
		//-----------------------------------------------------------------------------
	default:
		break;
	}

}
